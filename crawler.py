import urllib
import re
from teams import teams
regex = re.compile(r'(<td *\w*=*"*\w*"*>)|(<\/td>)|(<\s*a[^>]*>(.*?))|(</a>)')

def get_roster(choice):
    players = []
    sock = urllib.urlopen(choice[1])
    htmlsource = sock.read()
    sock.close()

    # Isolate Player Table
    begin_idx = htmlsource.index('<td colspan="8">Team Roster</td>')
    end_idx = htmlsource.index('<div class="col-ad-160">')
    player_table = htmlsource[begin_idx:end_idx]

    # Split string into individual rows
    row_splits = filter(None, re.split('(<tr class="oddrow player)|(<tr class="evenrow player*)', player_table))[1:]

    # Split each row on <td> and <a> to isolate data
    for i in range(len(row_splits)):
        if len(row_splits[i]) > 25:
            new_split = filter(None, re.split('(<td *\w*=*"*\w*"*>)|(<\/td>)|(<\s*a[^>]*>(.*?))|(</a>)', row_splits[i]))
            player = {
                "name" : new_split[6],
                "number" : new_split[2],
                "position" : new_split[10],
                "age" : new_split[13],
                "height" : new_split[16],
                "weight" : new_split[19],
                "college" : new_split[22],
                "salary" : new_split[25]
            }
            # Cleaning data that wasn't available
            for key,value in player.items():
                if value == "&nbsp;":
                    player[key] = "N/A"
            players.append(player)

    # Formatting table
    print("\n"+"*"*30)
    print(choice[0])
    print("*"*30 + "\n")
    print("{:<8}{:<25}{:<10}{:<10}{:<8}{:<8}{:<30}{:<10}".format("NO.", "NAME", "POS", "AGE", "HT", "WT", "COLLEGE", "SALARY"))
    print("-" * 120)
    for player in players:
        print('{:<8}{:<25}{:<10}{:<10}{:<8}{:<8}{:<30}{:<10}'.format(player['number'],player['name'],player['position'],player['age'], player['height'], player['weight'], player['college'], player['salary']))
    text = raw_input("\nPress 'c' to continue or anything else to exit: ")
    if text == 'c':
        init()
    else:
        return

# Creates list of teams to choose from and wait for user input
def get_choice():
    print("-"*80)
    print("\nSelect a Team\n")
    print("-"*80)    
    count = 0
    for i in range(len(teams)):
        if type(teams[i]) == str:
            print("\n" + teams[i] + "\n" + "*" * 20)
        else:
            print("{}. {}".format(i,teams[i][0]))
    valid = False
    while not valid:
        text = raw_input("\nSelect Option or 'x' to exit: ")
        if text == 'x':
            return None
        try:
            choice = int(text)
            if choice >= len(teams) or choice < 0 or choice in [0,6,12,18,24,30]:
                raise ValueError()
            team = teams[choice]
            return team
        except ValueError as error:
            print("Not a valid entry, try again")

def init():
    choice = get_choice()
    if choice != None:
        get_roster(choice)

if __name__ == "__main__":
    init()